jest.dontMock("../index");
const solution = require("../index");

describe("Rating", ()=>{
    it("should return 100",()=>{
        const avgRatingArry = [7,7,7,7,7,7,7,7,7,7,7,7,7];
        expect(typeof solution).toBe('function');
        expect(solution(avgRatingArry)).toBe(100);
    })

    it("should return 14 as mininum rating",()=>{
        const avgRatingArry = [1,1,1,1,1,1,1,1,1,1,1,1,1];
        expect(typeof solution).toBe('function');
        expect(solution(avgRatingArry)).toBe(14);
    })

    it("should return 59",()=>{
        const avgRatingArry = [3,2,4,5,1,2,4,4,5,3,4,3,5,5,4,4,3,5,6,3,2,5,5,5,5,4,5,4,4,5,6,7,5];
        expect(typeof solution).toBe('function');
        expect(solution(avgRatingArry)).toBe(59);
    })
})