# Profile Rating 

## Install
- `npm install`

## User Story

Aisle has all kinds of users. There are users from different faiths, attractiveness, social status, etc. Users Like or Pass other users based on the profiles they browse.

Using this information write an algorithm for a 'rating system' which will give all users a 'desirability score'. The higher the 'desirability score' of a user, the more other users showed interest in that profile.

## Proposed Solution

The users will be asked to take an anonymous survey to rate the profile based on a likert scale. These scores are stored as an array and passed as an argument to the function

![sample scale](static/sampleScale.png)

The default base rating score is taken as 7 with reference to the above scale.

### Algorithm : 
- get the array of likeabilty score 
- get the base rating of the form
- calculate the average of the likeability score array
  - `averageScore = totalRatings / numberOfPeopleRated`
- calculate the desirability score
  - `desirabilityScore = (averageScore / baseRating) * 100`
- return `Math.round(desirabilityScore)`


### Advantages :
- Removes the need of the user to have more likes on the profile to be desirable.
- The rating is based on the user interactions, how much they enjoy the company and the ability to engage the other user(making an effort).
- Makes the user experience more humane rather than algorithmic.

### Limitations :
- The user cannot rate the profile 0. The minimum score of the profile is 1 to avoid `divisibility by 0` error
- Desirability score cannot be calculated by the number of users liked or passed the profile.
- Rating needs to be anonyomous to avoid the need for the user to lie to put themselves in the 
- positive light.


## Resources
- [Likert Scale](https://en.wikipedia.org/wiki/Likert_scale)


# Storing Photos in S3

## User Story

Thousands of photos are uploaded on Aisle every hour. We have 2 choices on how we can upload them into our S3 buckets - by (i) Base64 (ii) Multipart.

Which option would you pick? And how would you explain to the team the reason for your choice?

## Explanation
- Base64 is used to encrypt the binary data which needs to be sent through a form POST
  - Each Base64 digit represents 6 bits of data
  - Base64 version of a string or file will be at most `133%` of size of its source. 
    - Which is approximately `33%` increase in the size would result in increaseed storage costs.
  - The increase may be larger if the encoded data is small.
    -  For example, the string `"a"` with `length == 1` gets encoded to `"YQ=="` with `length == 4` a `300%` increase.
  
- multipart is used to send each value as a block of data.
  - The files are divided into a series of blocks before sending them to the source.
  - These blocks are then assembled at the destination.
  - We can upload these blocks parallely to improve the throughput
  - Smaller packet size minimizes the impact of restarting a failed upload due to network error.
  - Once we initiate the multipart, we can complete or stop. There is no expiry.

  
## Resources
- [Base64](https://developer.mozilla.org/en-US/docs/Glossary/Base64?source=post_page----------)
- [Multipart](https://docs.aws.amazon.com/AmazonS3/latest/dev/uploadobjusingmpu.html)
- [POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST)


# Free 10 likes

## User Story

Aisle has millions of users from over 150 countries. Each user gets 10 free daily Likes. Unused Likes are not carried forward to the next day.

We must refresh the number of daily Likes at 12:00 pm according to the user's local time. How would you go about doing this in a scalable way? No need to write code, simply explain to us in theory the backend logic in your own words.

## Proposed Solution
- get the date of the user by `new Date()`
- if the date is 12:00am
  - reinitialize the count of like to 10 `freeLikeCount = 10`

