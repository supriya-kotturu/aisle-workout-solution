// avgRatingArry : array of ratings anonymously given 
//                 to the user by the their matches
// baseRating : the max rating range = 7 if the range is
//              1-7 on the rating form

function rating(avgRatingArry, baseRating = 7) {
  const totalFormRating = avgRatingArry.reduce((s, e) => s + e, 0);
  const avgRating = totalFormRating / avgRatingArry.length;
  return Math.round((avgRating / baseRating) * 100);
}

module.exports = rating;
